﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UIServiceCore.Models
{
    public class Farmacist
    {
        public int ID { get; set; }
        public string Emer { get; set; }
        public string Mbiemer { get; set; }
        public string Username { get; set; }
        public string Pass { get; set; }
        public string Status { get; set; }
        public string Image { get; set; }

        public Farmacist()
        {

        }

        public Farmacist(string Emer, string Mbiemer, string Username, string Pass, string Status, string Image)
        {
            this.Emer = Emer;
            this.Mbiemer = Mbiemer;
            this.Username = Username;
            this.Pass = Pass;
            this.Status = Status;
            this.Image = Image;
        }
    }
}
