﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UIServiceCore.Models
{
    public class Pacient
    {
        public int ID { get; set; }
        public string emer { get; set; }
        public string mbiemer { get; set; }
        public string email { get; set; }
        public string username { get; set; }
        public string pass { get; set; }
        public string status { get; set; }
        public string image { get; set; }

        public Pacient()
        {

        }

        public Pacient(string Emer, string Mbiemer, string Email, string Username, string Pass, string Status, string Image)
        {
            this.emer = Emer;
            this.mbiemer = Mbiemer;
            this.email = Email;
            this.username = Username;
            this.pass = Pass;
            this.status = Status;
            this.image = Image;
        }

        
    }
}
