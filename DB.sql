USE [Labkurs2]
GO
/****** Object:  Table [dbo].[Doktor]    Script Date: 17.7.2020 21:40:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Doktor](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Emer] [varchar](50) NOT NULL,
	[Mbiemer] [varchar](50) NOT NULL,
	[Email] [varchar](50) NULL,
	[Username] [varchar](50) NOT NULL,
	[Pass] [varchar](50) NOT NULL,
	[Specializim] [varchar](50) NOT NULL,
	[Image] [image] NULL,
	[Status] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Doktor] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Farmacist]    Script Date: 17.7.2020 21:40:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Farmacist](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Emer] [varchar](50) NOT NULL,
	[Mbiemer] [varchar](50) NOT NULL,
	[Email] [varchar](50) NULL,
	[Username] [varchar](50) NOT NULL,
	[Pass] [varchar](50) NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[Image] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Farmacist] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FarmacistProdukt]    Script Date: 17.7.2020 21:40:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FarmacistProdukt](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Farmacist] [int] NOT NULL,
	[ID_Produkt] [int] NOT NULL,
 CONSTRAINT [PK_FarmacistProdukt] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Kategori]    Script Date: 17.7.2020 21:40:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Kategori](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Emer] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Kategori] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pacient]    Script Date: 17.7.2020 21:40:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pacient](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Emer] [varchar](50) NOT NULL,
	[Mbiemer] [varchar](50) NOT NULL,
	[Email] [varchar](50) NULL,
	[Username] [varchar](50) NOT NULL,
	[Pass] [varchar](50) NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[Image] [image] NOT NULL,
 CONSTRAINT [PK_Pacient] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Produkt]    Script Date: 17.7.2020 21:40:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Produkt](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Emer] [varchar](50) NOT NULL,
	[ID_Kategori] [int] NOT NULL,
	[DataSkadimit] [date] NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[Image] [image] NOT NULL,
 CONSTRAINT [PK_Produkt] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Shporta]    Script Date: 17.7.2020 21:40:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shporta](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Pacient] [int] NOT NULL,
	[ID_Produkt] [int] NOT NULL,
	[Totali] [money] NOT NULL,
 CONSTRAINT [PK_Shporta] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Termin]    Script Date: 17.7.2020 21:40:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Termin](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Doktor] [int] NOT NULL,
	[ID_Pacient] [int] NOT NULL,
	[Data] [datetime] NOT NULL,
 CONSTRAINT [PK_Termin] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Udhezim]    Script Date: 17.7.2020 21:40:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Udhezim](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Doktor] [int] NOT NULL,
	[ID_Pacient] [int] NOT NULL,
	[EshteShfytezu] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Udhezim] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FarmacistProdukt]  WITH CHECK ADD  CONSTRAINT [FK_FarmacistProdukt_Farmacist] FOREIGN KEY([ID_Farmacist])
REFERENCES [dbo].[Farmacist] ([ID])
GO
ALTER TABLE [dbo].[FarmacistProdukt] CHECK CONSTRAINT [FK_FarmacistProdukt_Farmacist]
GO
ALTER TABLE [dbo].[FarmacistProdukt]  WITH CHECK ADD  CONSTRAINT [FK_FarmacistProdukt_Produkt] FOREIGN KEY([ID_Produkt])
REFERENCES [dbo].[Produkt] ([ID])
GO
ALTER TABLE [dbo].[FarmacistProdukt] CHECK CONSTRAINT [FK_FarmacistProdukt_Produkt]
GO
ALTER TABLE [dbo].[Produkt]  WITH CHECK ADD  CONSTRAINT [FK_Produkt_Kategori] FOREIGN KEY([ID_Kategori])
REFERENCES [dbo].[Kategori] ([ID])
GO
ALTER TABLE [dbo].[Produkt] CHECK CONSTRAINT [FK_Produkt_Kategori]
GO
ALTER TABLE [dbo].[Shporta]  WITH CHECK ADD  CONSTRAINT [FK_Shporta_Pacient] FOREIGN KEY([ID_Pacient])
REFERENCES [dbo].[Pacient] ([ID])
GO
ALTER TABLE [dbo].[Shporta] CHECK CONSTRAINT [FK_Shporta_Pacient]
GO
ALTER TABLE [dbo].[Shporta]  WITH CHECK ADD  CONSTRAINT [FK_Shporta_Produkt] FOREIGN KEY([ID_Produkt])
REFERENCES [dbo].[Produkt] ([ID])
GO
ALTER TABLE [dbo].[Shporta] CHECK CONSTRAINT [FK_Shporta_Produkt]
GO
ALTER TABLE [dbo].[Termin]  WITH CHECK ADD  CONSTRAINT [FK_Termin_Doktor] FOREIGN KEY([ID_Doktor])
REFERENCES [dbo].[Doktor] ([ID])
GO
ALTER TABLE [dbo].[Termin] CHECK CONSTRAINT [FK_Termin_Doktor]
GO
ALTER TABLE [dbo].[Termin]  WITH CHECK ADD  CONSTRAINT [FK_Termin_Pacient] FOREIGN KEY([ID_Pacient])
REFERENCES [dbo].[Pacient] ([ID])
GO
ALTER TABLE [dbo].[Termin] CHECK CONSTRAINT [FK_Termin_Pacient]
GO
ALTER TABLE [dbo].[Udhezim]  WITH CHECK ADD  CONSTRAINT [FK_Udhezim_Doktor] FOREIGN KEY([ID_Doktor])
REFERENCES [dbo].[Doktor] ([ID])
GO
ALTER TABLE [dbo].[Udhezim] CHECK CONSTRAINT [FK_Udhezim_Doktor]
GO
ALTER TABLE [dbo].[Udhezim]  WITH CHECK ADD  CONSTRAINT [FK_Udhezim_Pacient] FOREIGN KEY([ID_Pacient])
REFERENCES [dbo].[Pacient] ([ID])
GO
ALTER TABLE [dbo].[Udhezim] CHECK CONSTRAINT [FK_Udhezim_Pacient]
GO
