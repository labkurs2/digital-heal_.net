﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UIService.Models
{
    public class Produkt
    {
        public int ID { get; set; }
        public string Emer { get; set; }
        public int ID_Kategori { get; set; }
        public DateTime DataSkadimit { get; set; }
        public string Status { get; set; }
        public string Image { get; set; }

        public Produkt()
        {

        }

        public Produkt(string Emer, int ID_Kategori, DateTime DataSkadimit, string Status, string Image)
        {
            this.Emer = Emer;
            this.ID_Kategori = ID_Kategori;
            this.DataSkadimit = DataSkadimit;
            this.Status = Status;
            this.Image = Image;
        }
    }
}