﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProduktService.Models
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base (options)
        {

        }

        public DbSet<Produkt> Produkt { get; set; }
        public DbSet<Shporta> Shporta { get; set; }
        public DbSet<Kategori> Kategori { get; set; }
    }
}
