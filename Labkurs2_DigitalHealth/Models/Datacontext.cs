﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DoctorService.Models
{
    public class DataContext : DbContext
    {

        public DataContext(DbContextOptions<DataContext> option) : base(option)
        {

        }

        public DbSet<Doktor> Doktor { get; set; }
        public DbSet<Udhezim> Udhezim { get; set; }
    }
}
