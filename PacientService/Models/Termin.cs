﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PacientService.Models
{
    public class Termin
    {
        public int ID { get; set; }
        public int ID_Doktor { get; set; }
        public int ID_Pacient { get; set; }
        public DateTime Data { get; set; }

        public Termin()
        {

        }

        public Termin(int ID_Doktor, int ID_Pacient, DateTime Data)
        {
            this.ID_Doktor = ID_Doktor;
            this.ID_Pacient = ID_Pacient;
            this.Data = Data;
        }

        public void EditTermin(Termin termin)
        {
            ID_Doktor = termin.ID_Doktor;
            ID_Pacient = termin.ID_Pacient;
            Data = termin.Data;
        }


    }
}
