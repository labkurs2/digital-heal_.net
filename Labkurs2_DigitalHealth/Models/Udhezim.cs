﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DoctorService.Models
{
    public class Udhezim
    {
        public int ID { get; set; }
        public int ID_Doktor { get; set; }
        public int ID_Pacient { get; set; }
        public string EshteShfytezu { get; set; }

        public Udhezim()
        {

        }

        public Udhezim(int ID_Doktor, int ID_Pacient, string EshteShfytezu)
        {
            this.ID_Doktor = ID_Doktor;
            this.ID_Pacient = ID_Pacient;
            this.EshteShfytezu = EshteShfytezu;
        }

        public void EditUdhezim(Udhezim udhezim)
        {
            ID_Pacient = udhezim.ID_Pacient;
            ID_Doktor = udhezim.ID_Doktor;
            EshteShfytezu = udhezim.EshteShfytezu;
        }
    }
}
