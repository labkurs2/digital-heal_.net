﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProduktService.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProduktService.Controllers
{
    [Route("api/[controller]")]
    public class ProduktController : Controller
    {
        DataContext context;

        public ProduktController(DataContext context)
        {
            this.context = context;
        }
        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<Produkt> Get()
        {
            return context.Produkt.ToList();
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public Produkt Get(int id)
        {
            return context.Produkt.Find(id);
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]Produkt value)
        {
            context.Produkt.Add(value);
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Produkt value)
        {
            Produkt FindProdukt = context.Produkt.Find(id);
            FindProdukt.EditProdukt(value);
            context.Produkt.Update(FindProdukt);
            context.SaveChanges();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            Produkt FindProdukt = context.Produkt.Find(id);
            context.Produkt.Remove(FindProdukt);
            context.SaveChanges();
        }
    }
}
