﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FarmacistService.Models
{
    public class FarmacistProdukt
    {
        public int ID { get; set; }
        public int ID_Farmacist { get; set; }
        public int ID_Produkt { get; set; }

        public FarmacistProdukt()
        {

        }

        public FarmacistProdukt(int ID_Farmacist, int ID_Produkt)
        {
            this.ID_Farmacist = ID_Farmacist;
            this.ID_Produkt = ID_Produkt;
        }

        public void EditFarmacist(FarmacistProdukt farmacistProdukt)
        {
            ID_Farmacist = farmacistProdukt.ID_Farmacist;
            ID_Produkt = farmacistProdukt.ID_Produkt;
        }

    }

    
}
