﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PacientService.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PacientService.Controllers
{
    [Route("api/[controller]")]
    public class PacientController : Controller
    {

        DataContext context;

        public PacientController(DataContext context)
        {
            this.context = context;
        }
        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<Pacient> Get()
        {
            return context.Pacient.Where(d => d.Status == "Aktive");
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public Pacient Get(int id)
        {
            return context.Pacient.Find(id);
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]Pacient value)
        {
            context.Pacient.Add(value);
            context.SaveChanges();
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Pacient value)
        {
            Pacient FindPacient = context.Pacient.Find(id);
            FindPacient.EditPacient(value);
            context.Pacient.Update(FindPacient);
            context.SaveChanges();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            Pacient FindPacient = context.Pacient.Find(id);
            context.Pacient.Remove(FindPacient);
        }
    }
}
