﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using UIService.ViewModels;
using UIServiceCore.Models;

namespace UIService.Controllers
{
    public class DashboardController : Controller
    {
        HttpClient httpClient = new HttpClient();
        

        DataContext context;

        public DashboardController(DataContext context)
        {
            this.context = context;
        }

        // GET: Dashboard
        public ActionResult Doktor()
        {
            if (session())
            {
                return View();
            }
            return Redirect("/Login/Doktor");
        }

        public async Task<ActionResult> Tables()
        {
            if (session())
            {
                var response = await httpClient.GetStringAsync(
                "http://localhost:54661/api/Pacient");

                List<Pacient> pacient = JsonConvert.DeserializeObject<List<Pacient>>(response);

                return View(pacient);
            }

            return Redirect("/Login/Doktor");
        }

        public ActionResult Farmacist()
        {
            return View();
        }

        UdhezimViewModel UdhezimViewModel;
        public async void GetPacient()
        {
            if (session())
            {
                int id2 = Convert.ToInt32(TempData["ID"]);


                var response = await httpClient.GetStringAsync(
                "http://localhost:54661/api/Pacient/" + id2);

                Pacient pacient = JsonConvert.DeserializeObject<Pacient>(response);

                UdhezimViewModel.Pacient = pacient;



            }

            
           
        }

        public bool session()
        {
            return HttpContext.Session.GetInt32("ID") != null;
        }

        [Route("/Dashboard/Udhezim/{id:int}")]
        public ActionResult GetInt(int id)
        {
            TempData["IdPacientit"] = id;
            return Redirect("/Dashboard/Udhezim/");
        }
        
        [Route("/Dashboard/Udhezim/")]
        public async Task<ActionResult> Udhezim()
        {
            if (session())
            {
                int id = Convert.ToInt32(TempData["IdPacientit"]);
                var response = await httpClient.GetStringAsync(
                "http://localhost:54661/api/Pacient/"+id);

                Pacient pacient = JsonConvert.DeserializeObject<Pacient>(response);

                ViewBag.Emer = pacient.emer;
                ViewBag.Mbiemer = pacient.mbiemer;
                ViewBag.Email = pacient.mbiemer;

                return View();
            }

            return Redirect("/Login/Doktor");
        }

        

        /*
        [Route("/Udhezim")]
        public ActionResult Udhezim()
        {
            if (session())
            {
                
                return View(UdhezimViewModel);
            }

            return Redirect("/Login/Doktor");
        }

        //Dahboardh udhezim ID

            /*
        
        [HttpGet]
        public ActionResult Udhezim(int id)
        {
            if (session())
            {
                if (GetPacient(id) != null)
                {
                    Task<Pacient> pacient1 = GetPacient(id);
                    Pacient pacient = pacient1.Result;

                    return View(pacient);
                }
            }

            return Redirect("/Login/Doktor");
        }

        */
    }
}