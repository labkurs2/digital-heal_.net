﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProduktService.Models
{
    public class Kategori
    {
        public int ID { get; set; }
        public string Emer { get; set; }

        public Kategori()
        {

        }

        public Kategori(string Emer)
        {
            this.Emer = Emer;
        }

        public void EditKategori(Kategori kategori)
        {
            Emer = kategori.Emer;
        }
    }
}
