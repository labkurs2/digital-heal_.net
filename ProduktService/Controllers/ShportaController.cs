﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProduktService.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProduktService.Controllers
{
    [Route("api/[controller]")]
    public class ShportaController : Controller
    {
        DataContext context;

        public ShportaController(DataContext context)
        {
            this.context = context;
        }

        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<Shporta> Get()
        {
            return context.Shporta.ToList();
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public Shporta Get(int id)
        {
            return context.Shporta.Find(id);
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]Shporta value)
        {
            context.Shporta.Add(value);
            context.SaveChanges();
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Shporta value)
        {
            Shporta FindShporta = context.Shporta.Find(id);
            FindShporta.EditShporta(value);
            context.Shporta.Update(FindShporta);
            context.SaveChanges();

        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            Shporta FshijShporta = context.Shporta.Find(id);
            context.Shporta.Remove(FshijShporta);
        }
    }
}
