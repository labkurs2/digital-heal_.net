﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FarmacistService.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FarmacistService.Controllers
{
    [Route("api/[controller]")]
    public class FarmacistProduktController : Controller
    {
        DataContext context;

        public FarmacistProduktController(DataContext context)
        {
            this.context = context;
        }
        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<FarmacistProdukt> Get()
        {
            return context.FarmacistProdukt.ToList();
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public FarmacistProdukt Get(int id)
        {
            return context.FarmacistProdukt.Find(id);
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]FarmacistProdukt value)
        {
            context.FarmacistProdukt.Add(value);
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]FarmacistProdukt value)
        {
            FarmacistProdukt FindFarmacistProdukt = context.FarmacistProdukt.Find(id);
            FindFarmacistProdukt.EditFarmacist(value);
            context.FarmacistProdukt.Update(value);
            context.SaveChanges();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            FarmacistProdukt FshijFarmacistProdukt = context.FarmacistProdukt.Find(id);
            context.FarmacistProdukt.Remove(FshijFarmacistProdukt);
            context.SaveChanges();
        }
    }
}
