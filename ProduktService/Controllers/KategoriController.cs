﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProduktService.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProduktService.Controllers
{
    [Route("api/[controller]")]
    public class KategoriController : Controller
    {

        DataContext context;

        public KategoriController(DataContext context)
        {
            this.context = context;
        }

        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<Kategori> Get()
        {
            return context.Kategori.ToList();
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public Kategori Get(int id)
        {
            return context.Kategori.Find(id);
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]Kategori value)
        {
            context.Kategori.Add(value);
            context.SaveChanges();
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Kategori value)
        {
            Kategori FindKategori = context.Kategori.Find(id);
            FindKategori.EditKategori(value);
            context.Kategori.Update(FindKategori);
            context.SaveChanges();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            Kategori FshijKategori = context.Kategori.Find(id);
            context.Kategori.Remove(FshijKategori);
            context.SaveChanges();
        }
    }
}
