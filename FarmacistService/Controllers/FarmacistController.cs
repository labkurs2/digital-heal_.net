﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FarmacistService.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FarmacistService.Controllers
{
    [Route("api/[controller]")]
    public class FarmacistController : Controller
    {
        DataContext context;

        public FarmacistController(DataContext context)
        {
            this.context = context;
        }
        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<Farmacist> Get()
        {
            return context.Farmacist.ToList();
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public Farmacist Get(int id)
        {
            return context.Farmacist.Find(id);
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]Farmacist value)
        {
            context.Farmacist.Add(value);
            context.SaveChanges();
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Farmacist value)
        {
            Farmacist FindFarmacist = context.Farmacist.Find(id);
            FindFarmacist.EditFarmacist(value);
            context.Farmacist.Update(FindFarmacist);
            context.SaveChanges();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            Farmacist FshijFarmacist = context.Farmacist.Find(id);
            context.Farmacist.Remove(FshijFarmacist);
            context.SaveChanges();
        }
    }
}
