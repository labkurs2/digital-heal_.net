﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UIService.Models
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }

        public DbSet<Pacient> Pacient { get; set; }
        public DbSet<Farmacist> Farmacist { get; set; }
        public DbSet<Doktor> Doktor { get; set; }
    }
}