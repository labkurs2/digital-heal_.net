﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FarmacistService.Models
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base (options)
        {

        }

        public DbSet<Farmacist> Farmacist { get; set; }
        public DbSet<FarmacistProdukt> FarmacistProdukt { get; set; }
    }
}
