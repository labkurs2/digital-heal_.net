﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PacientService.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PacientService.Controllers
{
    [Route("api/[controller]")]
    public class TerminController : Controller
    {
        DataContext context;

        public TerminController(DataContext context)
        {
            this.context = context;
        }
        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<Termin> Get()
        {
            return context.Termin.ToList();
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public Termin Get(int id)
        {
            return context.Termin.Find(id);
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]Termin value)
        {
            context.Termin.Add(value);
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Termin value)
        {
            Termin FindTermin = context.Termin.Find(id);
            FindTermin.EditTermin(value);
            context.Termin.Update(FindTermin);
            context.SaveChanges();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            Termin FindTermin = context.Termin.Find(id);
            context.Termin.Remove(FindTermin);
            context.SaveChanges();
        }
    }
}
