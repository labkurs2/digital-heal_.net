﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProduktService.Models
{
    public class Shporta
    {
        public int ID { get; set; }
        public int ID_Pacient { get; set; }
        public int ID_Produkt { get; set; }
        public decimal Totali { get; set; }

        public Shporta()
        {

        }

        public Shporta(int ID_Pacient, int ID_Produkt, decimal Totali)
        {
            this.ID_Pacient = ID_Pacient;
            this.ID_Produkt = ID_Produkt;
            this.Totali = Totali;
        }

        public void EditShporta(Shporta shporta)
        {
            ID_Pacient = shporta.ID_Pacient;
            ID_Produkt = shporta.ID_Produkt;
            Totali = shporta.Totali;
        }
    }
}
