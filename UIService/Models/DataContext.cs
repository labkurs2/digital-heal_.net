﻿using Microsoft.EntityFrameworkCore;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UIServiceCore.Models
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options): base (options)
        {

        }

        public DbSet<Doktor> Doktor { get; set; }
        public DbSet<Pacient> Pacient { get; set; }
        public DbSet<Farmacist> Farmacist { get; set; }
    }
}
