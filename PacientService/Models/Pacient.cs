﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PacientService.Models
{
    public class Pacient
    {
        public int ID { get; set; }
        public string Emer { get; set; }
        public string Mbiemer { get; set; }
        public string Email  { get; set; }
        public string Username { get; set; }
        public string Pass { get; set; }
        public string Status { get; set; }
        public byte[] Image { get; set; }

        public Pacient()
        {

        }

        public Pacient(string Emer, string Mbiemer, string Email, string Username, string Pass, string Status, byte[] Image)
        {
            this.Emer = Emer;
            this.Mbiemer = Mbiemer;
            this.Username = Username;
            this.Email = Email;
            this.Pass = Pass;
            this.Status = Status;
            this.Image = Image;
        }

        public void EditPacient(Pacient pacient)
        {
            Emer = pacient.Emer;
            Mbiemer = pacient.Mbiemer;
            Email = pacient.Email;
            Username = pacient.Username;
            Pass = pacient.Pass;
            Status = pacient.Status;
            Image = pacient.Image;
        }

    }
}
