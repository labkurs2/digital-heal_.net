﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DoctorService.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DoctorService.Controllers
{
    [Route("api/[controller]")]
    public class DoktorController : Controller
    {
        DataContext context;
        public DoktorController(DataContext context)
        {
            this.context = context;
        }
        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<Doktor> Get()
        {
            return context.Doktor.ToList();
        }

        [HttpGet("{username:alpha}")]
        public Doktor Get(string username)
        {
            return context.Doktor.Where(d => d.Username == username).FirstOrDefault<Doktor>();
        }

        // GET api/<controller>/5
        [HttpGet("{id:min(0)}")]
        public Doktor Get(int id)
        {
            return context.Doktor.Find(id);
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]Doktor value)
        {
            
            context.Doktor.Add(value);
            context.SaveChanges();
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Doktor value)
        {
            Doktor FindDoktor = context.Doktor.Find(id);
            FindDoktor.EditDoktor(value);
            context.Doktor.Update(FindDoktor);
            context.SaveChanges();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            Doktor FshijDoktor = context.Doktor.Find(id);
            context.Doktor.Remove(FshijDoktor);
            context.SaveChanges();
        }

        public static byte[] GetPhoto(string filePath)
        {
            FileStream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            BinaryReader reader = new BinaryReader(stream);

            byte[] photo = reader.ReadBytes((int)stream.Length);

            reader.Close();
            stream.Close();

            return photo;
        }
    }
}
