﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UIServiceCore.Models
{
    public class Doktor
    {
        public int ID { get; set; }
        public string Emer { get; set; }
        public string Mbiemer { get; set; }
        public string Username { get; set; }
        public string Pass { get; set; }
        public string Specializim { get; set; }
        public Byte[] Image { get; set; }
        public string Status { get; set; }

        public Doktor()
        {

        }

        public Doktor(string Emer, string Mbiemer, string Specializim, Byte[] Image, string Status)
        {
            this.Emer = Emer;
            this.Mbiemer = Mbiemer;
            this.Specializim = Specializim;
            this.Image = Image;
            this.Status = Status;
        }
    }
}
