﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using UIServiceCore.Models;

namespace UIService.Controllers
{
    public class UdhezimController : Controller
    {

        HttpClient HttpClient = new HttpClient();
        // GET: Udhezim
        public ActionResult Index()
        {
            return View();
        }

        

        // GET: Udhezim/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Udhezim/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Udhezim/Edit/5
        
        public async Task<ActionResult> Udhezim(int id)
        {
            var response = await HttpClient.GetStringAsync(
                "http://localhost:54661/api/Pacient/" + id);

            Pacient pacient = JsonConvert.DeserializeObject<Pacient>(response);
            return View(pacient);
        }

        // POST: Udhezim/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Udhezim/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Udhezim/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}