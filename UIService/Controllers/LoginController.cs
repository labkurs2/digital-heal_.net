﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Session;
using UIServiceCore.Models;



namespace UIService.Controllers
{
    public class LoginController : Controller
    {
        DataContext context;



        public LoginController(DataContext context)
        {
            this.context = context;
        }
        public IActionResult Login(string Username, string Pass, string lloji)
        {
            if (lloji == "Doktor")
            {
                return doktorLogin(Username, Pass);
            }
            else if (lloji == "Farmacist")
            {
                return farmacistLogin(Username, Pass);
            }
            else if (lloji == "Pacient")
            {
                return pacientLogin(Username, Pass);
            }
            else
            {
                return Redirect("Login/Doktor");
            }
        }

        public IActionResult doktorLogin(string Username, String Pass)
        {
            var doktor =
            context.Doktor.Where(d => d.Username == Username && d.Pass == Pass)
            .FirstOrDefault<Doktor>();

            if (doktor != null)
            {


                if (Username.ToLower() == doktor.Username.ToLower() && Pass.ToLower() == doktor.Pass.ToLower())
                {
                    HttpContext.Session.SetInt32("ID", doktor.ID);
                    return Redirect("/Dashboard/Doktor");
                }
                else
                {
                    return Redirect("/Login/Doktor");
                }
            }
            else
            {
                return Redirect("/Login/Doktor");
            }
        }

        public IActionResult pacientLogin(string Username, String Pass)
        {
            var pacient =
            context.Pacient.Where(d => d.username == Username && d.pass == Pass)
            .FirstOrDefault<Pacient>();

            if (pacient != null)
            {


                if (Username.ToLower() == pacient.username.ToLower() && Pass.ToLower() == pacient.pass.ToLower())
                {
                    HttpContext.Session.SetInt32("ID", pacient.ID);
                    return Redirect("/Dashboard/Pacient");
                }
                else
                {
                    return Redirect("/Login/Pacient");
                }
            }
            else
            {
                return Redirect("/Login/Pacient");
            }
        }

        public IActionResult farmacistLogin(string Username, String Pass)
        {
            var farmacist =
            context.Farmacist.Where(d => d.Username == Username && d.Pass == Pass)
            .FirstOrDefault<Farmacist>();

            if (farmacist != null)
            {


                if (Username.ToLower() == farmacist.Username.ToLower() && Pass.ToLower() == farmacist.Pass.ToLower())
                {
                    HttpContext.Session.SetInt32("ID", farmacist.ID);
                    return Redirect("/Dashboard/Farmacist");
                }
                else
                {
                    return Redirect("/Login/Farmacist");
                }
            }
            else
            {
                return Redirect("/Login/Farmacist");
            }
        }
        public IActionResult Doktor()
        {
            return View();
        }

        public IActionResult Pacient()
        {
            return View();
        }

        public IActionResult Farmacist()
        {
            return View();
        }

        [Route("ForgotPassword")]
        public IActionResult ForgotPassword()
        {
            return View();
        }
    }
}