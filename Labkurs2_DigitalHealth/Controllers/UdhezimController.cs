﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DoctorService.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DoctorService.Controllers
{
    [Route("api/[controller]")]
    public class UdhezimController : Controller
    {
        DataContext context;

        public UdhezimController(DataContext context)
        {
            this.context = context;
        }
        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<Udhezim> Get()
        {
            return context.Udhezim.ToList();
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public Udhezim Get(int id)
        {
            return context.Udhezim.Find(id);
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]Udhezim value)
        {
            context.Udhezim.Add(value);
            context.SaveChanges();
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Udhezim value)
        {
            Udhezim FindUdhezim = context.Udhezim.Find(id);
            FindUdhezim.EditUdhezim(value);
            context.Udhezim.Update(FindUdhezim);
            context.SaveChanges();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            Udhezim FindUdhezim = context.Udhezim.Find(id);
            context.Udhezim.Remove(FindUdhezim);
        }
    }
}
